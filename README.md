# Vapster

_vapster_ is a project grouping modern components to run a [Napster](https://en.wikipedia.org/wiki/Napster) compatible service.

## Components

* [controld](https://gitlab.com/vapster/controld) is a load balancer to run in front of _Napster_ main servers written in [V](https://vlang.io/).
* [vapd](https://gitlab.com/vapster/vapd) is a tiny _Napster_ main server written in [V](https://vlang.io/).